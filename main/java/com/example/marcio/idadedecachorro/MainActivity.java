package com.example.marcio.idadedecachorro;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button button;
    private EditText entrada;
    private TextView resultado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.buttonId);
        entrada = findViewById(R.id.editTextId);
        resultado = findViewById(R.id.resultadoId);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String idade = entrada.getText().toString();

                if(!idade.isEmpty()){
                    int idadeNum = Integer.parseInt(idade);
                    int resultadoFinal = idadeNum*7;
                    resultado.setText("A idade equivalente do cachorro é " + resultadoFinal + " anos");
                } else {
                    Context contexto = getApplicationContext();
                    String texto = "A idade deve ser informada!";
                    int duracao = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(contexto, texto,duracao);
                    toast.show();
                }
            }
        });
    }
}
